package mx.com.ideass.kb.reportes.api.abc.vehiculos;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@RestController
public class ApiRestVehiculos {
	
	Logger log = LogManager.getLogger(ApiRestVehiculos.class);
	
	@Autowired
	private TransaccionVehiculos transaccionVehiculos;

	@GetMapping(path="vehiculos/obtenerReporteVehicular")
	public ResponseEntity<byte[]> obtenerReporteVehicular() throws JRException {
		
		// Cargar la plantilla Jasper
		String templatePath = "/Ejemplo.jrxml";
		log.info(String.format("Invoice template path : %s", templatePath));

	    final InputStream reportInputStream = getClass().getResourceAsStream(templatePath);
	    final JasperDesign jasperDesign = JRXmlLoader.load(reportInputStream);

	    // Compilar el reporte de Jasper
	    JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
		
	    // Obtener los datos del reporte
	    List<DtoVehiculo> listaDtoVehiculo = transaccionVehiculos.obtenerVehiculos();
	    JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(listaDtoVehiculo);
		Map<String, Object> parameters = new HashMap<String, Object>();

		// Extraccion del reporte PDF en un arreglo de bytes
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);
        JasperFillManager.fillReport(jasperReport, parameters, ds);
		byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
		
		// Configuracion de las cabeceras de respuesta
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		String filename = "ReporteVehicular.pdf";
		headers.setContentDispositionFormData(filename, filename);
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		    
		// Creacion de la respuesta con el cuerpo del arreglo de bytes y
		// la cabeceras para interpretar el arreglo como un archivo PDF
		ResponseEntity<byte[]> response = new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		
		return response;
	}
}
