package mx.com.ideass.kb.reportes.api.abc.vehiculos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TransaccionVehiculos {

	public List<DtoVehiculo> obtenerVehiculos() {
		 
		List<DtoVehiculo> listaDtoVehiculo = new ArrayList<DtoVehiculo>();
		
		DtoVehiculo dtoVehiculo = new DtoVehiculo();
		dtoVehiculo.setId(1l);
		dtoVehiculo.setNombre("Vento");
		dtoVehiculo.setPrecio(189000);
		
		listaDtoVehiculo.add(dtoVehiculo);
		
		dtoVehiculo = new DtoVehiculo();
		dtoVehiculo.setId(2l);
		dtoVehiculo.setNombre("City");
		dtoVehiculo.setPrecio(260000);
		
		listaDtoVehiculo.add(dtoVehiculo);
		
		dtoVehiculo = new DtoVehiculo();
		dtoVehiculo.setId(3l);
		dtoVehiculo.setNombre("Dodge");
		dtoVehiculo.setPrecio(360000);
		
		listaDtoVehiculo.add(dtoVehiculo);
		
		return listaDtoVehiculo;
	}
}
